from azure.cognitiveservices.vision.customvision.training import CustomVisionTrainingClient
from azure.cognitiveservices.vision.customvision.prediction import CustomVisionPredictionClient
from msrest.authentication import ApiKeyCredentials
import os

ENDPOINT = "https://gundulicatest.cognitiveservices.azure.com/"
ENDPOINT_PREDICTOR = "https://gundulicatest-prediction.cognitiveservices.azure.com/"
training_key = "55535c4546c34cb3b435363f493aa429"
prediction_key = "37599295b115424ea3596b03126ce954"
prediction_resource_id = "/subscriptions/b7e99573-35c0-4a15-b805-9337d102e344/resourceGroups/GudeljTestVision/providers/Microsoft.CognitiveServices/accounts/GundulicaTest-Prediction"

credentials = ApiKeyCredentials(in_headers={"Training-key": training_key})
trainer = CustomVisionTrainingClient(ENDPOINT, credentials)
prediction_credentials = ApiKeyCredentials(in_headers={"Prediction-key": prediction_key})
predictor = CustomVisionPredictionClient(ENDPOINT_PREDICTOR, prediction_credentials)

publish_iteration_name = "TestModel"

credentials = ApiKeyCredentials(in_headers={"Training-key": training_key})
trainer = CustomVisionTrainingClient(ENDPOINT, credentials)
base_image_location = os.path.dirname(__file__)

project = trainer.get_project("12d37ff4-5be4-497f-bb8b-f517e473e50e")

def loadImage(path):
    with open(os.path.join (base_image_location, path), "rb") as image_contents:
        results = predictor.classify_image(project.id, publish_iteration_name, image_contents.read())
    for prediction in results.predictions:
        return prediction.tag_name

from tkinter import *
from tkinter.ttk import *
from tkinter import filedialog

import prediction

def choose():
    path = list(filedialog.askopenfilenames(filetypes=[('Images','*.jpg *.jpeg *.png')]))
    lbl.configure(text=path)

def getImagePath():
    imagePath = lbl.cget("text")
    return imagePath

def predict():
    fruitName = prediction.loadImage(getImagePath())
    lblResult.configure(text=fruitName)
    lblFruit = Label(window, text="Fruit: ")
    lblFruit.place(x=165, y=220)

window = Tk()
window.title("Fruit Classification")
window.geometry("500x300")
window.frame()

lbl = Label(window, text="")
lbl.place(x=10, y=50)

btnChoose = Button(window, text="Choose a photo", command=choose)
btnChoose.place(x=180, y=80)

btnPredict = Button(window, text="Predict", command=predict)
btnPredict.place(x=190, y=150)

lblResult = Label(window, text="")
lblResult.place(x=200, y=220)

window.mainloop()